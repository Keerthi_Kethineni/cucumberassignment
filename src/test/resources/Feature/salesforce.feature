Feature: Verify the Login Functionality

@tag1
Scenario Outline: Verify login with valid username and blank password for Testcase1

Given user is on login page
When user enter value into the textbox "<username>" and "<password>"
When user click on the Login Button 
Then check the alert Error Please enter your password.


 Examples:
| username | password |
| keerthi@kk.com | | 

@tag2
Scenario Outline: Verify login with valid username and password with Check Remember me for Testcase2

Given user is on login page
When user enter value into the textbox "<username>" and "<password>"
When user click on the Remember me checkbox
When user click on the Login Button
Given user is on home page
When user click on the User Menu Button
When user click on the Logout Button 
Then user returns to LoginPage dispalying Username in the username field

 Examples:
| username | password |
| keerthi@kk.com | keetu@123 |

@tag3
Scenario Outline: Verify login with valid username and password for Testcase3

Given user is on login page
When user enter value into the textbox "<username>" and "<password>"  
When user click on the Login Button
Then check the User Name on HomePage

Examples:
| username | password |
| keerthi@kk.com | keetu@123 |

@tag4
Scenario Outline: Verify User Menu drop down for Testcase4
Given user is on login page
When user enter value into the textbox "<username>" and "<password>"  
When user click on the Login Button
Given user is on home page
When user click on the User Menu Button 
Then check for the User Menu display

Examples:
| username | password |
| keerthi@kk.com | keetu@123 |

@tag5
Scenario Outline: Verify Pawssword reset link for Testcase5

Given user is on login page 
When user click on the forgot password link
When user enter value into the "<Username1>"
When user click on the Continue Button
Then check for the Message Check Your Email

Examples:
| Username1 |
| keerthi@kk.com |


