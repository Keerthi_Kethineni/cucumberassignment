package com.tekarch.cucumberNew.Assignment;

import java.awt.AWTException;
import java.awt.HeadlessException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.Select;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

public class BaseTest {
	protected static WebDriver driver;
	static protected ExtentReports reports;
	protected ExtentTest logger;

	static final String TEST_REPORTS = "/Users/sreenivaskuppuru/Documents/Keerthi/QA/Test-Reports/";

	static String pattern = "MM/dd/yyyy";
	static SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

	public static void initializeDriver() {
		System.setProperty("webdriver.chrome.driver", "/Users/sreenivaskuppuru/Selenium/chromedriver3");
		ChromeOptions chromeOptions = new ChromeOptions();
		chromeOptions.addArguments("--headless");
		
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();

	}

	public static void cleanUpResources() {
		reports.flush();
		driver.close();
	}

	public void setInput(WebElement inputElement, String inputValue) {
		if (inputElement.isDisplayed()) {
			inputElement.sendKeys(inputValue);
			System.out.println(inputValue + "entered" );
		} else {
			System.out.println(inputElement + "is not displayed.");
		}
	}

	public void selectField(Select selectElement, String selectValue) {
		selectElement.selectByVisibleText(selectValue);
		System.out.println(selectValue + "is selected.");
	}

	public void click(WebElement buttonElement, String buttonName) {
		if (buttonElement.isDisplayed()) {
			buttonElement.click();
			System.out.println(buttonElement + "is clicked.");
		} else {
			System.out.println(buttonName + "is not displayed");
		}
	}

	public void clickCheckbox(WebElement checkboxElement, String checkBoxName) {
		if (checkboxElement.isDisplayed()) {
			checkboxElement.click();
			System.out.println(checkBoxName + " clicked.");
		} else {
			System.out.println(checkBoxName + " is not displayed");
		}
	}

	public static void initializeReport() {
		reports = new ExtentReports(
				TEST_REPORTS + new SimpleDateFormat("'SalesForceReport_'YYYYMMddHHmmss'.html'").format(new Date()));
	}

	public String takeScreenshot() throws IOException {
		TakesScreenshot srcShot = ((TakesScreenshot) driver);
		File srcFile = srcShot.getScreenshotAs(OutputType.FILE);
		String imagePath = TEST_REPORTS + "ScreenShots/"
				+ new SimpleDateFormat("'Image_'YYYYMMddHHmmss'.png'").format(new Date());
		File destFile = new File(imagePath);
		FileUtils.copyFile(srcFile, destFile);
		return imagePath;
	}

	public String takePopupScreenshot() throws HeadlessException, AWTException, IOException {
		BufferedImage image = new Robot()
				.createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
		String imagePath = TEST_REPORTS + "ScreenShots/"
				+ new SimpleDateFormat("'Image_'YYYYMMddHHmmss'.png'").format(new Date());
		ImageIO.write(image, "png", new File(imagePath));

		return imagePath;
	}
}
