package com.tekarch.cucumberNew.Assignment;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)

@CucumberOptions(format = {"pretty", "html:target/CucumberReport", "json:target/CucumberReport/cucumber.json"},
		features = "/Users/sreenivaskuppuru/Documents/workspace/CucumberAssignment/src/test/resources/Feature/salesforce.feature"
		,glue={"com.tekarch.cucumberNew.Assignment"},
		tags = {}
 
)
public class RunTest {

}
