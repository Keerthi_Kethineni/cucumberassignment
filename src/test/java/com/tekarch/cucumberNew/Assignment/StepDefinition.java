package com.tekarch.cucumberNew.Assignment;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.relevantcodes.extentreports.LogStatus;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StepDefinition extends BaseTest {

	@Before
	public void setUp() {
		initializeDriver();
		initializeReport();

	}

	@After
	public void cleanup() {
		cleanUpResources();
	}

	@Given("^user is on login page$")
	public void user_is_on_login_page() throws Throwable {
		driver.get("https://login.salesforce.com/");
		logger = reports.startTest("Login");
		logger.log(LogStatus.INFO, " Test case Started");

	}

	@When("^user enter value into the textbox \"([^\"]*)\" and \"([^\"]*)\"$")
	public void user_enter_value_into_the_textbox_and(String usrNme, String pswd) throws Throwable {
		WebElement userName = driver.findElement(By.xpath("//input[@id='username']"));
		setInput(userName, usrNme);
		logger.log(LogStatus.INFO, "Username is entered successfully");
		WebElement password = driver.findElement(By.xpath("//input[@id='password']"));
		setInput(password, pswd);
		logger.log(LogStatus.INFO, "Password is entered successfully");
	}

	@When("^user click on the Login Button$")
	public void user_click_on_the_Login_Button() throws Throwable {
		WebElement loginButton = driver.findElement(By.xpath("//input[@id='Login']"));
		click(loginButton, "Login Button");
		logger.log(LogStatus.INFO, "Login Button Clicked successfully");
		Thread.sleep(1000);
	}

	@Then("^check the alert Error Please enter your password\\.$")
	public void check_the_alert_Error_Please_enter_your_password() throws Throwable {
		WebElement alertMessage = driver.findElement(By.xpath("//div[@id='error']"));
		String error = alertMessage.getText();
		Assert.assertEquals(error, "Please enter your password.");
		logger.log(LogStatus.INFO,
				"Login failure and see the attached screenshot. \r\n" + logger.addScreenCapture(takeScreenshot()));
		Thread.sleep(1000);
		reports.endTest(logger);
	}

	@When("^user click on the Remember me checkbox$")
	public void user_click_on_the_Remember_me_checkbox() throws Throwable {
		WebElement rememberMe = driver.findElement(By.xpath("//input[@id='rememberUn']"));
		clickCheckbox(rememberMe, "Remember Me");
		logger.log(LogStatus.INFO, "Checked Remember Me successfully");
		Thread.sleep(1000);

	}

	@Given("^user is on home page$")
	public void user_is_on_home_page() throws Throwable {

	}

	@When("^user click on the User Menu Button$")
	public void user_click_on_the_User_Menu_Button() throws Throwable {
		WebElement uMenuDropDown = driver.findElement(By.xpath("//div[@id='userNavButton']"));
		click(uMenuDropDown, "User Menu Drop Down Button");
		logger.log(LogStatus.INFO, "User Menu Drop Down Button clicked successfully");
		Thread.sleep(1000);
	}

	@When("^user click on the Logout Button$")
	public void user_click_on_the_Logout_Button() throws Throwable {
		WebElement SignoutButton = driver.findElement(By.xpath("//a[contains(text(),'Logout')]"));
		click(SignoutButton, "Sign Out Button");
		logger.log(LogStatus.INFO, "Sign Out Button clicked successfully");
		Thread.sleep(1000);

	}

	@Then("^user returns to LoginPage dispalying Username in the username field$")
	public void user_returns_to_LoginPage_dispalying_Username_in_the_username_field() throws Throwable {
		WebElement displayUsername = driver.findElement(By.xpath("//span[@id='idcard-identity']"));
		Assert.assertEquals(displayUsername.getText(), "keerthi@kk.com");
		Thread.sleep(2000);
		logger.log(LogStatus.INFO,
				"Username is Displayed in the Username Field. \r\n" + logger.addScreenCapture(takeScreenshot()));
		reports.endTest(logger);
	}

	@Then("^check the User Name on HomePage$")
	public void check_the_User_Name_on_HomePage() throws Throwable {
		WebElement checkUsername = driver.findElement(By.xpath("//span[@id='userNavLabel']"));
		String uName = checkUsername.getText();
		Assert.assertEquals(uName, "Keerthi Kethineni");
		Thread.sleep(3000);
		logger.log(LogStatus.INFO,
				"Login successful and see the attached screenshot. \r\n" + logger.addScreenCapture(takeScreenshot()));
		reports.endTest(logger);
	}

	@Then("^check for the User Menu display$")
	public void check_for_the_User_Menu_display() throws Throwable {
		WebElement displayUsermenu = driver.findElement(By.xpath("//div[@id='userNavMenu']"));
		Assert.assertTrue(displayUsermenu.isDisplayed());
		logger.log(LogStatus.INFO, "User Menu Drop Down Displayed successfully");
		Thread.sleep(2000);
		logger.log(LogStatus.INFO, "Drop down is displayed and see the attached screenshot. \r\n"
				+ logger.addScreenCapture(takeScreenshot()));
		reports.endTest(logger);
	}

	@When("^user click on the forgot password link$")
	public void user_click_on_the_forgot_password_link() throws Throwable {
		WebElement forgotPasswordLink = driver.findElement(By.xpath("//a[@id='forgot_password_link']"));
		click(forgotPasswordLink, "Password Reset Link");
		logger.log(LogStatus.INFO, "Password Reset Link Clicked successfully");
		Thread.sleep(1000);
	}

	@When("^user enter value into the \"([^\"]*)\"$")
	public void user_enter_value_into_the(String usrNme1) throws Throwable {
		WebElement userName1 = driver.findElement(By.xpath("//input[@id='un']"));
		setInput(userName1, usrNme1);
		logger.log(LogStatus.INFO, "Username is entered successfully");
		Thread.sleep(500);
	}

	@When("^user click on the Continue Button$")
	public void user_click_on_the_Continue_Button() throws Throwable {
		WebElement continueButton = driver.findElement(By.xpath("//input[@id='continue']"));
		click(continueButton, "Continue Button");
		logger.log(LogStatus.INFO, "Continue Button Clicked successfully");
		Thread.sleep(500);
	}

	@Then("^check for the Message Check Your Email$")
	public void check_for_the_Message_Check_Your_Email() throws Throwable {
		WebElement checkMail = driver.findElement(By.xpath("//div[@id='main']"));
		// checkMail.isDisplayed();
		Assert.assertTrue(checkMail.isDisplayed());
		Thread.sleep(1000);
		logger.log(LogStatus.INFO, "Password reset link has been sent to mail and see the attached screenshot. \r\n"
				+ logger.addScreenCapture(takeScreenshot()));
		reports.endTest(logger);
	}

}
